import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BarChartComponent } from './component/bar-chart/bar-chart.component';
import { DoughnutChartComponent } from './component/doughnut-chart/doughnut-chart.component';
import { PieChartComponent } from './component/pie-chart/pie-chart.component';
import { HomePageComponent } from './component/home-page/home-page.component';

const routes: Routes = [
    {path:'bar-charts',component: BarChartComponent},
    {path:'doughnut-chart',component: DoughnutChartComponent},
    {path:'pie-chart',component: PieChartComponent},
    {path:'home-page',component: HomePageComponent},
    {path:'**',component: HomePageComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
